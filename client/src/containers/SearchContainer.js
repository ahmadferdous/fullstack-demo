import React, { Component } from 'react';
import axios from 'axios';

import SearchForm from '../components/SearchForm';
import SearchResults from '../components/SearchResults';

// Time is short. Hack!
const SERVER_ADDRESS = 'http://localhost:4000';


export default class SearchContainer extends Component {
  constructor() {
    super();
    this.state = {
      searchComplete: false,
      results: [],
    };
  }

  findHotels = async (city, checkin, checkout) => {
    const results = await axios.get(
      SERVER_ADDRESS + '/hotels',
      {
        params: {
          city,
          checkin,
          checkout,
        }
      }
    );

    this.setState({
      searchComplete: true,
      results: results.data,
    });
  };

  render() {
    let searchResults = <div />;
    if (this.state.searchComplete) {
      searchResults = <SearchResults results={this.state.results}/>
    }
    return (
      <div className="container-fluid">
        <SearchForm onSearch={this.findHotels} />
        <br />
        <br />
        {searchResults}
      </div>
    );
  }
}
