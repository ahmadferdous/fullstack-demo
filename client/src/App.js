import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import SearchContainer from './containers/SearchContainer';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <SearchContainer/>
      </div>
    );
  }
}

export default App;
