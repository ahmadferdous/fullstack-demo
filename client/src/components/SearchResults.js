import React, { Component } from 'react';

export default class SearchResults extends Component {
  getHeader() {
    return (
      <thead>
        <tr>
          <th scope="col">Hotel ID</th>
          <th scope="col">Hotel Name</th>
          <th scope="col">Number of reviews</th>
          <th scope="col">Address</th>
          <th scope="col">Number of Stars</th>
          <th scope="col">Amenities</th>
          <th scope="col">Image</th>
          <th scope="col">Snaptravel Price</th>
          <th scope="col">Hotel.com Price</th>
        </tr>
      </thead>
    );
  }

  getRow(result) {
    const {
      id,
      hotel_name,
      num_reviews,
      address,
      num_stars,
      amenities,
      image_url,
      price,
      retailPrice: retail_price,
    } = result;
    return (
      <tr key={id}>
        <td>{id}</td>
        <td>{hotel_name}</td>
        <td>{num_reviews}</td>
        <td>{address}</td>
        <td>{num_stars}</td>
        <td>{amenities.join('\n')}</td>
        <td>
          <img src={image_url} />
        </td>
        <td>{price}</td>
        <td>{retail_price}</td>
      </tr>
    );
  }

  render() {
    const rows = (
      <tbody>
      {this.props.results.map((r) => this.getRow(r))}
      </tbody>
    );
    return (
      <table className="table table-bordered">
        {this.getHeader()}
        {rows}
      </table>
    );
  }
}
