import React, { Component } from 'react';

export default class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: '',
      checkin: '',
      checkout: '',
    };
  }

  onCityChange = (event) => {
    this.setState({
      city: event.target.value,
    });
  };

  onCheckinChange = (event) => {
    this.setState({
      checkin: event.target.value,
    });
  };

  onCheckoutChange = (event) => {
    this.setState({
      checkout: event.target.value,
    });
  };

  onSearch = (event) => {
    event.preventDefault();
    this.props.onSearch(
      this.state.city,
      this.state.checkin,
      this.state.checkout,
    );
  };

  render() {
    return (
      <form onSubmit={this.onSearch}>
        <input
          type="text"
          value={this.state.city}
          onChange={this.onCityChange}
          className="form-control"
          placeholder="Enter city name"
        />

        <input
          type="text"
          value={this.state.checkin}
          onChange={this.onCheckinChange}
          className="form-control"
          placeholder="Checkin date"
        />

        <input
          type="text"
          value={this.state.checkout}
          onChange={this.onCheckoutChange}
          className="form-control"
          placeholder="Checkout date"
        />

        <button type="submit" className="btn btn-primary">
          Find Hotels
        </button>
      </form>
    );
  }
}
