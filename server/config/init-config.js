'use strict';

const nconf = require('nconf');
const DefaultConfig = require('./defaults');

nconf.use('memory');
nconf.argv().env();
nconf.defaults(DefaultConfig);
