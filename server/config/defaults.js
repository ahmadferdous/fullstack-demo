'use strict';

module.exports = {
  API_SERVER_BASE_URL: 'https://experimentation.getsnaptravel.com/interview',
  REDIS_URL: 'redis://localhost:6379',
  SNAPTRAVEL_RESULTS_TTL_SECONDS: 5,
  RETAIL_RESULTS_TTL_SECONDS: 10,
};
