'use strict';

const redis = require('redis');

const nconf = require('nconf');
const REDIS_URL = nconf.get('REDIS_URL');

const client = redis.createClient(REDIS_URL);
client.on('error', (err) => {
  console.error(err);
});

const { promisify } = require('util');
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);

module.exports = {
  getAsync,
  setAsync,
};
