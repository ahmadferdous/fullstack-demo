'use strict';

const request = require('request-promise-native');
const nconf = require('nconf');
const BASE_URL = nconf.get('API_SERVER_BASE_URL');

const Cache = require('./cache');

const PROVIDER = {
  SNAPTRAVEL: 'snaptravel',
  RETAIL: 'retail',
};

const TTL = {
  [PROVIDER.SNAPTRAVEL]: nconf.get('SNAPTRAVEL_RESULTS_TTL_SECONDS'),
  [PROVIDER.RETAIL]: nconf.get('RETAIL_RESULTS_TTL_SECONDS'),
};

async function findHotelsForProvider(provider, city, checkin, checkout) {
  let results = await Cache.get(city, checkin, checkout, provider);
  if (!results) {
    const jsonBody = {
      city,
      checkin,
      checkout,
      provider,
    };

    const data = await request.post(
      BASE_URL + '/hotels',
      {
        body: jsonBody,
        json: true,
      }
    );

    results = data.hotels;
    await Cache.save(city, checkin, checkout, provider, results, TTL[provider]);

    console.log(`${provider} results served from API`);
  } else {
    console.log(`${provider} results served from cache`);
  }

  return results;
}

function mergeResults(snapTravelResults, retailResults) {
  // Doing O(n^2) to KISS here. Can be done in O(n) with additional space.
  const results = [];
  snapTravelResults.forEach((snapTravelResult) => {
    const retailResult = retailResults.find((r) => r.id === snapTravelResult.id);
    if (retailResult) {
      snapTravelResult.retailPrice = retailResult.price;
    }
  });

  // Assuming price is positive.
  return snapTravelResults.filter((result) => result.price && result.retailPrice);
}

async function findHotels(city, checkin, checkout) {
  const snapTravelResults = await findHotelsForProvider(
    PROVIDER.SNAPTRAVEL,
    city,
    checkin,
    checkout,
  );

  const retailResults = await findHotelsForProvider(
    PROVIDER.RETAIL,
    city,
    checkin,
    checkout,
  );

  return mergeResults(snapTravelResults, retailResults);
}

module.exports = {
  findHotels,
};
