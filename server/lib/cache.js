'use strict';

const RedisClient = require('./clients/redis-client');

async function save(city, checkin, checkout, provider, results, ttlSeconds) {
  console.log(ttlSeconds);
  await RedisClient.setAsync(
    buildKey(city, checkin, checkout, provider),
    JSON.stringify(results),
    'EX',
    ttlSeconds
  );
}

async function get(city, checkin, checkout, provider) {
  const results  = await RedisClient.getAsync(
    buildKey(city, checkin, checkout, provider)
  );

  return results ? JSON.parse(results) : results;
}

function buildKey(city, checkin, checkout, provider) {
  const delimiter = ':';
  return [city, checkin, checkout, provider].join(delimiter);
}

module.exports = {
  save,
  get,
};
