'use strict';

const express = require('express');
const router = express.Router();

const { findHotels } = require('../lib/hotel-finder');

router.get('/', async function(req, res, next) {
  try {
    const { city, checkin, checkout } = req.query;
    const hotels = await findHotels(city, checkin, checkout);
    res.json(hotels);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
